function [res] = besjn_li(v, x, n)

if (x<0)  , error('Argument less than zero!'), end
if (x>2*n), error('Argument should be less than 2n!'), end

res = 0;
for m = 0:n
    b = (-1)^m* factorial(n+m-1)* n^(1-2*m) ...
        /factorial(m)/factorial(n-m)/gamma(v+m+1);
    res = res+ b* (x./2).^(2*m+v);
end

end