function [res] = besjn_mil(v, x, n)

if (x<0)  , error('Argument less than zero!'), end
if (x>2*n), error('Argument should be less than 2n!'), end

res = 0;
for m = v:n
    b = (-1)^(m+v)* 2^(v-2*m) *n^(1-2*m) *factorial(n+m-1) ...
        /factorial(m)/factorial(n-m)/factorial(m-v);
    res = res+ b* (x).^(2*m-v);
end

end