clc; close all; clear all;

% minimal 
eps0 = 8.85418781762038985054D-12;
miu0 = 1.25663706143591729539D-06;

fmin  = 1;
fmax = 100e6;
amin = 1e-21;
amax =0.3;

ka_min = amin  *fmin *2*pi* sqrt(eps0*miu0);
ka_max = amax *fmax* 2*pi* sqrt(eps0*miu0);

x  = linspace(ka_min, ka_max, 251);
x2 = linspace(ka_min, ka_max, 1001);

bj0 = besselj(0, x);
bj1 = besselj(1, x);
bj0_cos = bj0.*cos(x);
bj1_cos = bj1.*cos(x);
bj0_sin = bj0.*sin(x);
bj1_sin = bj1.*sin(x);

bj0_li = besjn_li(0, x2, 50);
bj0_nw = besj0_nw(x2);
bj0_mil = besjn_mil(0, x2, 80);
bj0_sp = spline(x, bj0, x2);
figure
plot(x, bj0, x2, bj0_sp, '-o', x2, bj0_mil, '-x')
% plot(x, bj0, x, bj0_cos, x, bj0_sin)
grid on; box on; hold on;
%plot( x, bj1,  '-o', x, bj1_cos, '-o', x, bj1_sin, '-o')

set(gca, 'FontSize', 15);
xlabel('Argument range');
ylabel('Function Value');
legend('BesselJ_0', 'BesselJ_0 COS', 'BesselJ_0 SIN', 'BesselJ_1', 'BesselJ_1 COS', 'BesselJ_1 SIN')

% Difference
bj0_c = besselj(0, x2);
diff = abs(bj0_c-bj0_sp);
figure
plot(x2, diff)